# import
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
import csv

numberQueries = 226 #max. 226
top = 15

data = []
for d in range(1400):
    f = open("./d/" + str(d + 1) + ".txt")
    data.append(f.read())

queries = []
for q in range(1, numberQueries):
    f = open("./q/" + str(q) + ".txt")
    queries.append(f.read())

relevant = []
relevant.append([])
for r in range(1, numberQueries):
    f = open("./r/" + str(r) + ".txt")
    relevant.append([])
    for line in f:
        relevant[r].append(int(line))


def euclidean(query, data):
    distances = np.array(euclidean_distances(query, data)[0])
    topRelevant = distances.argsort() + 1

    return topRelevant

def cosine(query, data):
    sim = np.array(cosine_similarity(query, data)[0])
    topRelevant = sim.argsort()[::-1] + 1

    return topRelevant

def precision(retrieved, relevant):
    relevant_count = 0
    false_count = 0
    for doc in retrieved:
        if doc in relevant:
            relevant_count += 1
        else:
            false_count += 1

    return relevant_count / (relevant_count + false_count)


def recall(retrieved, relevant):
    relevant_count = 0
    for doc in retrieved:
        if doc in relevant:
            relevant_count += 1

    return relevant_count / len(relevant)


def fmeasure(precision, recall):
    if precision == 0 and recall == 0:
        return 0
    return 2 * (precision * recall) / (precision + recall)


with open('output.csv', mode='w') as f:
    writer = csv.writer(f, delimiter=';')
    csv_row = ['Query #',
               'Binary euclidean - precision',
               'Binary cosine - precision',
               'Binary euclidean - recall',
               'Binary cosine - recall',
               'Binary euclidean - F-measure',
               'Binary cosine - F-measure',
               'TF euclidean - precision',
               'TF cosine - precision',
               'TF euclidean - recall',
               'TF cosine - recall',
               'TF euclidean - F-measure',
               'TF cosine - F-measure',
               'TF-IDF euclidean - precision',
               'TF-IDF cosine - precision',
               'TF-IDF euclidean - recall',
               'TF-IDF cosine - recall',
               'TF-IDF euclidean - F-measure',
               'TF-IDF cosine - F-measure',
            ]
    writer.writerow(csv_row)

    for i, query in enumerate(queries, 1):
        data.append(query)

        print('Query #{}'.format(i))
        # binary
        binary_vectorizer = CountVectorizer(binary=True)
        binary_matrix = binary_vectorizer.fit_transform(data)
        binary_query = binary_matrix[len(data) - 1]
        binary_data = binary_matrix[0:len(data) - 1]

        binary_euclidean = euclidean(binary_query, binary_data)
        binary_cosine = cosine(binary_query, binary_data)

        binary_euclidean_precision = precision(binary_euclidean[:top], relevant[i])
        binary_euclidean_recall = recall(binary_euclidean[:top], relevant[i])
        binary_euclidean_fmeasure = fmeasure(binary_euclidean_precision, binary_euclidean_recall)

        binary_cosine_precision = precision(binary_cosine[:top], relevant[i])
        binary_cosine_recall = recall(binary_cosine[:top], relevant[i])
        binary_cosine_fmeasure = fmeasure(binary_cosine_precision, binary_cosine_recall)

        print("Binary representation + Euclidean distance")
        print("Precision: ", binary_euclidean_precision)
        print("Recall: ", binary_euclidean_recall)
        print("F-measure: ", binary_euclidean_fmeasure)
        print("-------------------------------")

        print("Binary representation + Cosine similarity measure")
        print("Precision: ", binary_cosine_precision)
        print("Recall: ", binary_cosine_recall)
        print("F-measure: ", binary_cosine_fmeasure)
        print("-------------------------------")

        # term frequency
        freq_vectorizer = TfidfVectorizer(use_idf=False)
        freq_matrix = freq_vectorizer.fit_transform(data)
        freq_query = freq_matrix[len(data) - 1]
        freq_data = freq_matrix[0:len(data) - 1]

        freq_euclidean = euclidean(freq_query, freq_data)
        freq_cosine = cosine(freq_query, freq_data)

        freq_euclidean_precision = precision(freq_euclidean[:top], relevant[i])
        freq_euclidean_recall = recall(freq_euclidean[:top], relevant[i])
        freq_euclidean_fmeasure = fmeasure(freq_euclidean_precision, freq_euclidean_recall)

        freq_cosine_precision = precision(freq_cosine[:top], relevant[i])
        freq_cosine_recall = recall(freq_cosine[:top], relevant[i])
        freq_cosine_fmeasure = fmeasure(freq_cosine_precision, freq_cosine_recall)

        print("Term Frequency + Euclidean distance")
        print("Precision: ", freq_euclidean_precision)
        print("Recall: ", freq_euclidean_recall)
        print("F-measure: ", freq_euclidean_fmeasure)
        print("-------------------------------")

        print("Term Frequency + Cosine similarity measure")
        print("Precision: ", freq_cosine_precision)
        print("Recall: ", freq_cosine_recall)
        print("F-measure: ", freq_cosine_fmeasure)
        print("-------------------------------")

        # tfidf
        tfidf_vectorizer = TfidfVectorizer()
        tfidf_matrix = tfidf_vectorizer.fit_transform(data)
        tfidf_query = tfidf_matrix[len(data) - 1]
        tfidf_data = tfidf_matrix[0:len(data) - 1]

        tfidf_euclidean = euclidean(tfidf_query, tfidf_data)
        tfidf_cosine = cosine(tfidf_query, tfidf_data)

        tfidf_euclidean_precision = precision(tfidf_euclidean[:top], relevant[i])
        tfidf_euclidean_recall = recall(tfidf_euclidean[:top], relevant[i])
        tfidf_euclidean_fmeasure = fmeasure(tfidf_euclidean_precision, tfidf_euclidean_recall)

        tfidf_cosine_precision = precision(tfidf_cosine[:top], relevant[i])
        tfidf_cosine_recall = recall(tfidf_cosine[:top], relevant[i])
        tfidf_cosine_fmeasure = fmeasure(tfidf_cosine_precision, tfidf_cosine_recall)

        print("TF-IDF + Euclidean distance")
        print("Precision: ", tfidf_euclidean_precision)
        print("Recall: ", tfidf_euclidean_recall)
        print("F-measure: ", tfidf_euclidean_fmeasure)
        print("-------------------------------")

        print("TF-IDF + Cosine similarity measure")
        print("Precision: ", tfidf_cosine_precision)
        print("Recall: ", tfidf_cosine_recall)
        print("F-measure: ", tfidf_cosine_fmeasure)
        print("-------------------------------")

        csv_row = [
                i,
                binary_euclidean_precision,
                binary_cosine_precision,
                binary_euclidean_recall,
                binary_cosine_recall,
                binary_euclidean_fmeasure,
                binary_cosine_fmeasure,
                freq_euclidean_precision,
                freq_cosine_precision,
                freq_euclidean_recall,
                freq_cosine_recall,
                freq_euclidean_fmeasure,
                freq_cosine_fmeasure,
                tfidf_euclidean_precision,
                tfidf_cosine_precision,
                tfidf_euclidean_recall,
                tfidf_cosine_recall,
                tfidf_euclidean_fmeasure,
                tfidf_cosine_fmeasure
            ]
        writer.writerow(csv_row)

        data.pop()
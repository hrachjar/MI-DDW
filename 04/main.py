import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
import matplotlib.pyplot as plt
import csv

def createGraph(inputFile):
    db = {}
    G = nx.Graph()
    G.name = "Actors"

    with open(inputFile, 'r', encoding='utf-8') as csvfile:
        text = csv.reader(csvfile, delimiter=';', quotechar='"')
        for row in text:
            movie = row[1]

            if movie not in db:
                actors = []
            else:
                actors = db[movie]

            if row[2] == '':
                actor = 's a'
            else:
                actor = row[2]

            actors.append(actor)
            db[movie] = actors

        for k, v in db.items():
            for actor in v:
                G.add_node(actor)
                for actor2 in v:
                    if actor != actor2:
                        G.add_edge(actor,actor2)

    return(G)

def saveGraph(G):
    nx.write_gexf(G, "actors.gexf")

def getInfo(G):
    #print(G.edges(data=True))
    print(nx.info(G))
    print("Density:", nx.density(G))
    print("Connected:", nx.is_connected(G))
    print("Number of connected components:", nx.number_connected_components(G))
    print("Average degree connectivity of graph", nx.average_degree_connectivity(G))

def bacon(G):
    distance = {}
    sum = 0
    count = 0

    for actor in G:
        if (nx.has_path(G, actor, 'Kevin Bacon')):
            d = nx.shortest_path_length(G, actor, 'Kevin Bacon')
            distance[actor] = d
            sum += d
            count += 1

    baconNumber = sum/count

    print("Bacon number:", baconNumber)
    print("Bacon distance:", distance)

G = createGraph("casts.csv")
getInfo(G)
bacon(G)
#saveGraph(G)

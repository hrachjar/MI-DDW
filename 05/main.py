from collections import Counter

import pandas as pd
import scipy


def frequentItems(transactions, support):
    counter = Counter()
    for trans in transactions:
        counter.update(frozenset([t]) for t in trans)
    return set(item for item in counter if counter[item] / len(transactions) >= support), counter


def generateCandidates(L, k):
    candidates = set()
    for a in L:
        for b in L:
            union = a | b
            if len(union) == k and a != b:
                candidates.add(union)
    return candidates


def filterCandidates(transactions, itemsets, support):
    counter = Counter()
    for trans in transactions:
        subsets = [itemset for itemset in itemsets if itemset.issubset(trans)]
        counter.update(subsets)
    return set(item for item in counter if counter[item] / len(transactions) >= support), counter


def apriori(transactions, support):
    result = list()
    resultc = Counter()
    candidates, counter = frequentItems(transactions, support)
    result += candidates
    resultc += counter
    k = 2
    while candidates:
        candidates = generateCandidates(candidates, k)
        candidates,counter = filterCandidates(transactions, candidates, support)
        result += candidates
        resultc += counter
        k += 1
    resultc = {item:(resultc[item]/len(transactions)) for item in resultc}
    return result, resultc

def generateSides(itemset: frozenset):
    itemlist = list(itemset)
    res = []
    for item in itemlist:
        newlist = list(itemlist)
        newlist.remove(item)
        res.append((newlist, item))
    return res

def generateRules(frequentItemsets, supports, minConfidence, sortByConfidence=False):
    rules = []

    for itemset in frequentItemsets:
        if len(itemset) < 2:
            continue

        for entry in generateSides(itemset):
            X, Y = entry
            rule_confidence = supports[itemset] / supports[frozenset(X)]
            rule_lift = supports[itemset] / (supports[frozenset(X)] * supports[frozenset([Y])])
            denominator = 1-supports[itemset] / supports[frozenset(X)]
            if denominator:
                rule_conviction = (1-supports[frozenset([Y])]) / (denominator)
            else:
                rule_confidence = 0

            if rule_confidence >= minConfidence:
                rules.append((X, Y, rule_confidence, rule_lift, rule_conviction))

    if sortByConfidence:
        rules = sorted(rules, key=lambda rule: rule[2], reverse=True)

    for rule in rules:
        X = rule[0]
        Y=rule[1]
        rule_confidence=rule[2]
        print(X, "=>",Y)
        print("confidence:\t", rule_confidence)
        print("lift:\t\t", rule_lift)
        print("conviction:\t", rule_conviction)
        print("------------------")


def getDataset(data):
    dataset = []
    sessions = set(data["VisitID"])
    for visitID in sessions:
        visitPages = data[data.VisitID == visitID]
        row = []
        for visitPage in visitPages.to_dict(orient="records"):
            row.append((visitPage["CatName"], visitPage["PageName"]))
        dataset.append(row)

    return dataset


#basic statistics
clicks = pd.read_csv("./clicks.csv")
visitors = pd.read_csv("./visitors.csv")
sem = pd.read_csv('./search_engine_map.csv')
print("==========================")
print("Basic statistics")
print("==========================")
print("visitors:", len(visitors))
print("clicks:", len(clicks))

avgTime = scipy.mean(clicks["TimeOnPage"])
print("average time on page:", avgTime)

days = Counter(visitors["Day"]).most_common()
print("top days:", days)

countPageName = Counter(clicks["PageName"])
print("top pages:", countPageName.most_common(5))

#preprocessing
#all tables in data
data = clicks.merge(visitors, on='VisitID', how="left", )
data = data.merge(sem, on='Referrer', how="left", )

dataNew = data
dataNew = dataNew[data.Length_seconds > 10]
dataNew = dataNew[dataNew.Length_pagecount > 3]
dataNew = dataNew[dataNew.ExtCatID != -1]

#del dataNew["VisitID"]
del dataNew["Referrer"]
print("==========================")
print("New data after preprocessing")
print("==========================")
print("total click:", len(dataNew))
print("unique pages:", len(Counter(dataNew["PageID"])))
print("unique visitors:", len(Counter(dataNew["VisitID"])))
print("==========================")
print("Conversion")
print("==========================")
countPageName = Counter(dataNew["PageName"])

for pageName, count in countPageName.items():
    if (pageName == "APPLICATION" or pageName == "CATALOG" or pageName == "DISCOUNT" or pageName == "HOWTOJOIN" or pageName == "INSURANCE" or pageName == "WHOWEARE"):
        print(pageName, count)


dataset = getDataset(dataNew)
minConfidence = 0.05

frequentItemsets, supports = apriori(dataset, 0.01)

print("==========================")
print("Association rules")
print("==========================")

generateRules(frequentItemsets, supports, minConfidence, sortByConfidence=True)
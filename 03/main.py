from string import punctuation
import nltk
import wikipedia

def get_data():
    with open('text.txt', 'r', encoding='utf-8') as f:
        text = f.read()

    return text

def extractEntities(ne_chunked):
    data = {}
    for entity in ne_chunked:
        if isinstance(entity, nltk.tree.Tree):
            text = " ".join([word for word, tag in entity.leaves()])
            ent = entity.label()
            data[text] = ent
        else:
            continue
    return data

def pos_tagging(text):
    sentences = nltk.sent_tokenize(text)
    tokens = [nltk.word_tokenize(sent) for sent in sentences]
    tagged = [nltk.pos_tag(sent) for sent in tokens]

    return tagged

def named_entities(text):
    tokens = nltk.word_tokenize(text)
    tagged = nltk.pos_tag(tokens)
    ne_chunked = nltk.ne_chunk(tagged)

    data = {}
    for entity in ne_chunked:
        if isinstance(entity, nltk.tree.Tree):
            text = " ".join([word for word, tag in entity.leaves()])
            ent = entity.label()
            data[text] = ent
        else:
            continue

    return data

def custom_ner(text):
    tokens = nltk.word_tokenize(text)
    tagged = nltk.pos_tag(tokens)

    entity = []
    entities = []
    for tagged_entry in tagged:
        if (tagged_entry[1].startswith("NN") or (entity and tagged_entry[1].startswith("IN"))):
            entity.append(tagged_entry)
        else:
            if (entity and entity[-1][1].startswith("IN")):
                entity.pop()
            if (entity and " ".join(e[0] for e in entity)[0].isupper()):
                entities.append(" ".join(e[0] for e in entity))
            entity = []

    entities = list(set(entities))
    return entities

def wiki(words):

    data = {}
    for word in words:
        results = wikipedia.search(word,results=1)
        if (results):
            try:
                page = wikipedia.page(results)
                sentence = page.summary
            except wikipedia.exceptions.DisambiguationError as e:
                try:
                    page = wikipedia.page(e.options[0])
                    sentence = page.summary
                except wikipedia.exceptions.DisambiguationError as e:
                    sentence = "Thing"
                    data[word] = sentence

            tokens = nltk.word_tokenize(page.summary)
            nopunc_tokens = [token for token in tokens if token not in punctuation]
            text_pos = nltk.pos_tag(nopunc_tokens)
            grammar = "NP: {<DT>?<JJ>*<NN|NNS>}"

            cp = nltk.RegexpParser(grammar)
            resultX = cp.parse(text_pos)

            entity = []
            for tagged_entry in resultX:
                if (isinstance(tagged_entry, nltk.tree.Tree)):
                    entity.append(" ".join([wordA[0] for wordA in tagged_entry.leaves()]))
                    break

            data[word] = entity[0]
            print(word, entity)

        else:
            sentence = "Thing"
            data[word] = sentence

    return data


####################################

text = get_data()

# POS tagging
tagged = pos_tagging(text)
print(tagged)

# NER with entity classification
ne_chunked_entities = named_entities(text)
print(ne_chunked_entities)

# NER with custom patterns
custom_entities = custom_ner(text)
print(custom_entities)

# Wiki
definition = wiki(ne_chunked_entities)
print(definition)